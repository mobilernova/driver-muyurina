import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.admmuyurina.starter',
  appName: 'Seguritaxi Conductor',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
