import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { AlertController, LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';

import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: any;
  password: any;
  success: any;
  player_id: string;

  constructor(private service:ApiService,

  public loadingController:LoadingController,
  public toast:ToastController,
  public router:Router,
  public nav:NavController,
  public alertController:AlertController,
  private androidPermissions:AndroidPermissions,
  public menu:MenuController) {
    this.menu.enable(false);
    this.player_id = localStorage.getItem("player_id");
  }

  ngOnInit() {
    console.log(this.player_id);
    this.confirmUpdate();
  }

  async goPhoneMovile() {
    this.player_id = localStorage.getItem("player_id");
    const loading = await this.loadingController.create({
      spinner: 'bubbles'
    });
    await loading.present().then(() => {
      this.service.loginCliente(this.email, this.password,this.player_id).subscribe(
        async data => {
          this.success = data.data;
          localStorage.setItem('token', data.token);
          localStorage.setItem('id', this.success.id);
          localStorage.setItem("trackingAuth","true");

          console.log(this.success);
          this.ok();
          this.nav.navigateRoot(["/home"]);
          await loading.dismiss();
        }, err => {
          console.log(err.error.error);
          this.smsError(err.error.error);
          loading.dismiss();
        }
      );
    });
  }

  async ok() {
    const toast = await this.toast.create({
      message: 'Bienvenido a AMD-MOV',
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  goRegister(){
    // async loadLocate(){
      const data = this.androidPermissions.requestPermissions([
        "android.permission.ACCESS_BACKGROUND_LOCATION",
        "android.permission.ACCESS_COARSE_LOCATION",
        this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
      ]);
      this.router.navigate(["/register"]);

    }
  // }

  async confirmUpdate() {
    const alert = await this.alertController.create({
      header: 'Mensaje',
      message: '"Esta aplicación recopila datos de ubicación para habilitar el seguimiento en tiempo real del Driver',
      buttons: [{
        text: 'ok',
        cssClass: 'secondary',
        handler: (blah) => {
         this.loadLocate();
        }
      }]
    });

    await alert.present();
  }
  async loadLocate(){
    const data = this.androidPermissions.requestPermissions([
      "android.permission.ACCESS_BACKGROUND_LOCATION",
      "android.permission.ACCESS_COARSE_LOCATION",
      this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
    ]);
  }



}
