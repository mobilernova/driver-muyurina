import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from '../../service/api.service';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  successRegister: any;
  img = "assets/img/logo.png";
  fotoDefault: string;
  player_id: string;
  form: FormGroup;
  string: number;

  constructor(private service:ApiService,
  public loadingController:LoadingController,
  public toast:ToastController,
  public actionSheetController:ActionSheetController,
  private camera: Camera,
  public formBuilder: FormBuilder,
  public nav:NavController
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      // password: ['', Validators.compose([Validators.required])],
      nombre: ['', Validators.compose([Validators.required])],
      licencia: ['', Validators.compose([Validators.required])],
      domicilio: ['', Validators.compose([Validators.required])],
      edad: ['', Validators.compose([Validators.required])],
      nombre_movil: ['', Validators.compose([Validators.required])],
      ci: ['', Validators.compose([Validators.required])],

      numero_cel: ['', Validators.compose([Validators.required, Validators.max(79999999), Validators.min(59999999)])],

    });
  }

  numberOnlyValidation(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  onChangeTime(e) {
    this.string = (8 - e.detail.value.length);
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
  async goRegister() {
    this.player_id = localStorage.getItem("player_id");
    const loading = await this.loadingController.create({
      spinner: 'bubbles'
    });
    await loading.present().then(() => {
      this.service.registerCliente(this.form.value.nombre,this.form.value.edad,this.form.value.domicilio,this.form.value.email,this.form.value.ci,this.form.value.numero_cel,this.form.value.licencia,this.form.value.nombre_movil,this.player_id,this.fotoDefault).subscribe(
        async data => {
          console.log(data);
          
          this.successRegister = data.data;
          localStorage.setItem('token', data.token);
          localStorage.setItem('id', this.successRegister.id);
          console.log(this.successRegister);
          this.ok();
          this.nav.navigateRoot(["/home"]);
          await loading.dismiss();
        }, err => {
          //  
          console.log(err);
          if (err.error.errors.player_id == 'The player id has already been taken.') {
            this.smsError("La cuenta esta registrada en otro dispositivo eliminar dispositivo con administradora.");
            loading.dismiss();
          } else {
            console.log(err.error.error);
            this.smsError("Email, Carnet de identidad o Número de celular ya fueron tomados");
            loading.dismiss();
          }
        
        }
      );
    });
  }


  async updatePhoto() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Sacar fotografia',
      buttons: [{
        text: 'Camara',
        icon: 'camera',
        handler: () => {
          this.takephoto();
        }
      }, {
        text: 'Galeria',
        icon: 'image',
        handler: () => {
          this.getimage();
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

  takephoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020
    }

    this.camera.getPicture(options).then((imageData) => {
      this.fotoDefault = "data:image/jpeg;base64," + imageData;
      // this.updatePhotoClient(this.photo);
    }, (err) => {
      console.log(err);

    });
  }
  getimage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020
    }

    this.camera.getPicture(options).then((imageData) => {
      this.fotoDefault = "data:image/jpeg;base64," + imageData;
      // this.updatePhotoClient(this.photo);
    }, (err) => {
      console.log(err);

    });
  }


  async ok() {
    const toast = await this.toast.create({
      message: 'Bienvenido a AMD-MOV',
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 5000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

}
