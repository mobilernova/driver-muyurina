import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./access/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./access/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'asigned-request',
    loadChildren: () => import('./driver/asigned-request/asigned-request.module').then( m => m.AsignedRequestPageModule)
  },
  {
    path: 'cars',
    loadChildren: () => import('./driver/cars/cars.module').then( m => m.CarsPageModule)
  },
  {
    path: 'detail-request',
    loadChildren: () => import('./driver/detail-request/detail-request.module').then( m => m.DetailRequestPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./driver/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'informacion',
    loadChildren: () => import('./driver/informacion/informacion.module').then( m => m.InformacionPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./driver/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./driver/profile/profile.module').then( m => m.ProfilePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
