import { Component } from '@angular/core';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { AppUpdate } from '@robingenz/capacitor-app-update';
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Inicio', url: 'home', icon: 'home' },
    { title: 'Automoviles', url: 'cars', icon: 'car-sport' },
    { title: 'Solicitudes', url: 'asigned-request', icon: 'document-attach' },
    { title: 'Notificaciones', url: 'notifications', icon: 'notifications' },
    { title: 'Información', url: 'informacion', icon: 'information' },
  ];
  datates: string;
  lat: any;
  lng: any;
  constructor(private appVersion: AppVersion,
  private oneSignal: OneSignal,
  private platform: Platform,
  private nav:NavController,
  private androidPermissions:AndroidPermissions,
  public alertController:AlertController,
  private geolocation: Geolocation) { 
    this.init();
  }

  

  init(){
    this.platform.ready().then(() => {
      // const data = this.androidPermissions.requestPermissions([
      //   "android.permission.ACCESS_BACKGROUND_LOCATION",
      //   "android.permission.ACCESS_COARSE_LOCATION",
      //   "android.permission.ACTIVITY_RECOGNITION",
      //   this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
      // ]);
      this.oneSignal.startInit('2d5d0a0c-8e94-4b61-b550-e032f207be86', '864974766888');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
      this.oneSignal.endInit();
      this.oneSignal.getIds().then(data => {
        console.log(data);
        
        localStorage.setItem('player_id', data.userId);
      });
      localStorage.removeItem('idRestaurante');

      // this.loadUserEnabled();
      // this.statusBar.backgroundColorByHexString('#34b04a96');
      // this.splashScreen.hide();
      if (localStorage.getItem('id')) {
        console.log("entre al tru");
        
        // this.idRepartidor = localStorage.getItem('idRepartidor');
        this.nav.navigateRoot('/home');
        // this.nombres = localStorage.getItem('nombres');
      } else {
        console.log("entre al false");
  
        this.nav.navigateRoot('/login');
      }
    });
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log("hola entre al geolocation");
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      localStorage.setItem("lat",   this.lat)   
      localStorage.setItem("lng", this.lng)       
      resp.coords.longitude
      console.log(resp.coords.latitude,resp.coords.longitude);
      
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
    console.log("entre a la funcion");
    
    // this.appVersion.getVersionNumber().then((version) => {
    //   this.datates = version;
    //   console.log(this.datates);
      
      // this.service.getVersion().subscribe((data) => {
      //   console.log(data);
        
        // if (data.data.nombre != this.datates) {
        //   console.log("entre a la condicion");
          
        //   navigator['app'].exitApp();
        //   var appId = "com.instagram.android";
        //   cordova.plugins.market.open(appId)
        // }
      // });
    // });
    // AppUpdate.getAppUpdateInfo().then(appUpdateInfo => {
    //   console.log(appUpdateInfo);
    //   if (appUpdateInfo.updateAvailability == 1) {
    //     this.presentAlert();
    //   }
    //   // const result = await AppUpdate.getAppUpdateInfo();
    //   // return result.availableVersion;
    // });  
  }



  
  async initApp() {
    console.log("entre a init app");
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Información',
      // subHeader: 'Subtitle',
      message: 'Hay una versión reciente de la aplicación actualizalo para tener las nuevas funciones.',
      buttons: [ {
        text: 'Ok',
        handler: () => {
          console.log('Confirm Okay');
          this.appUpdateInfo();
          // this.alertController.dismiss();
        }
      }]
    });
  
    await alert.present();
  }
  async  appUpdateInfo(){
    AppUpdate.openAppStore();
  }
}
