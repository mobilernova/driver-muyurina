import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';


import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { CallNumber } from '@awesome-cordova-plugins/call-number/ngx';
import { BackgroundGeolocation} from '@awesome-cordova-plugins/background-geolocation/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@awesome-cordova-plugins/launch-navigator/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { Dialogs } from '@awesome-cordova-plugins/dialogs/ngx';
import { ActionSheet, ActionSheetOptions } from '@awesome-cordova-plugins/action-sheet/ngx';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { AppAvailability } from '@awesome-cordova-plugins/app-availability/ngx';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';


import { initializeApp } from "firebase/app";
const firebaseConfig = {
  apiKey: "AIzaSyAASv9iIpEUgYSB7TxqioyNPsGKY5UZLCs",
  authDomain: "taxis-37f7b.firebaseapp.com",
  databaseURL: "https://taxis-37f7b-default-rtdb.firebaseio.com",
  projectId: "taxis-37f7b",
  storageBucket: "taxis-37f7b.appspot.com",
  messagingSenderId: "864974766888",
  appId: "1:864974766888:web:fffa578b143514b2f1461d",
  measurementId: "G-4K9PZFNNQM"
};


initializeApp(firebaseConfig);
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [HttpClientModule,BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [Geolocation,InAppBrowser,AppAvailability,Camera,BackgroundGeolocation,LaunchNavigator,ActionSheet,Dialogs,CallNumber,AndroidPermissions,AppVersion,OneSignal,{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
