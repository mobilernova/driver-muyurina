import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsignedRequestPage } from './asigned-request.page';

const routes: Routes = [
  {
    path: '',
    component: AsignedRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsignedRequestPageRoutingModule {}
