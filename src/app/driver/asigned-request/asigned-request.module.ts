import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsignedRequestPageRoutingModule } from './asigned-request-routing.module';

import { AsignedRequestPage } from './asigned-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsignedRequestPageRoutingModule
  ],
  declarations: [AsignedRequestPage]
})
export class AsignedRequestPageModule {}
