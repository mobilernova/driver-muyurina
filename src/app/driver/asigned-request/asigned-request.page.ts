import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-asigned-request',
  templateUrl: './asigned-request.page.html',
  styleUrls: ['./asigned-request.page.scss'],
})
export class AsignedRequestPage implements OnInit {
  selectOrder = "pendiente";
  successRequestDriverActive: any;
  idDriver: any;
  successRequestDriverWait: any;
  successRequestDriverHistoric: any;
  successDriver: any;
  controlEnabled: boolean;
  successRequest: any;
  

  constructor(private service:ApiService, 
  public toast:ToastController,
  public router:Router,
  public nav:NavController,
  public alertController:AlertController) { 
    this.idDriver = localStorage.getItem("id");
  }

  ngOnInit() {
    this.loadProfile();
    this.loadRequest();
  }

  async loadProfile() {
    this.service.getUser(this.idDriver).subscribe(
      async data => {
        this.successDriver = data.data;

        if (this.successDriver.estado === "Habilitado") {
          this.controlEnabled = true;
        } else {
          this.controlEnabled = false;
        }
      }, err => {
        console.log(err.error.error);
      })
  }

  
  segmentChanged(e){
    console.log(e);
    if (e.detail.value == "pendiente") {
      let mode = "Asignadas";
      this.service.getsolicitudes(this.idDriver, mode).subscribe(
        async data => {
          this.successRequestDriverActive = data.data;
          console.log(this.successRequestDriverActive);
        }, err => {
          console.log(err.error.error);
          this.smsError(err.error.error);
          // loading.dismiss();
        })
    } else if(e.detail.value == "en_espera"){
      let mode = "En espera";

      this.service.getsolicitudes(this.idDriver, mode).subscribe(
        async data => {
          this.successRequestDriverWait = data.data;

          console.log(this.successRequestDriverWait);
        }, err => {
          console.log(err.error.error);
          this.smsError(err.error.error);
          // loading.dismiss();
        })
    } else if(e.detail.value == "historicos"){
      let mode = "Historicos";

      this.service.getsolicitudes(this.idDriver, mode).subscribe(
        async data => {
          this.successRequestDriverHistoric = data.data;

        }, err => {
          console.log(err.error.error);
          this.smsError(err.error.error);
          // loading.dismiss();
        })
    }
  }

  loadRequest(){
    let mode = "Asignadas";
      this.service.getsolicitudes(this.idDriver, mode).subscribe(
        async data => {
          this.successRequestDriverActive = data.data;

          console.log(this.successRequestDriverActive);
        }, err => {
          console.log(err.error.error);
          this.smsError(err.error.error);
          // loading.dismiss();
        })
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  goRequest(item){
    this.router.navigateByUrl("/detail-request", {
      state: {
        data: item
      }
    });
  }

  notifications(){
    this.nav.navigateRoot(["/notifications"]);
  }


  async acceptRequest(item) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Deseas aceptar la carrera?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.service.acceptsolicitudes(item.id_solicitud, this.idDriver).subscribe(
              async data => {
                this.successRequest = data.data;
                this.selectOrder = "pendiente";
                this.loadRequest();
                console.log(this.successRequest);
              }, err => {
                console.log(err.error.error);
                this.smsError(err.error.error);
                // loading.dismiss();
              })
          }
        }
      ]
    });
  
    await alert.present();
  }

}
