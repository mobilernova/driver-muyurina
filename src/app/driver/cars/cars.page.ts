import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { CreatePage } from './create/create.page';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.page.html',
  styleUrls: ['./cars.page.scss'],
})
export class CarsPage implements OnInit {
  successCarDriver: any;
  idDriver: string;
  successDriver: any;
  controlEnabled: boolean;

  constructor(private service: ApiService,
    public toast: ToastController,
    public alertController: AlertController,
    public modalController: ModalController,
    public nav: NavController) {
    this.idDriver = localStorage.getItem("id");

  }

  ngOnInit() {
    this.loadProfile();
    this.loadCars();
  }

  async loadProfile() {
    this.service.getUser(this.idDriver).subscribe(
      async data => {
        this.successDriver = data.data;

        if (this.successDriver.estado === "Habilitado") {
          this.controlEnabled = true;
        } else {
          this.controlEnabled = false;
        }
      }, err => {
        console.log(err.error.error);
      })
  }

  async loadCars() {
    this.service.getTransportes(this.idDriver).subscribe(
      async data => {
        this.successCarDriver = data.data;
        console.log(this.successCarDriver);

      }, err => {
        console.log(err.error.error);
        this.smsError(err.error.error);
        // loading.dismiss();
      })
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  async selectMovil(item) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Quiere seleccionar este auto para que el cliente pueda ver el vehiculo que maneja?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.service.updateTransporteSelect(this.idDriver, item.id).subscribe(
              async data => {
                this.successCarDriver = data.data;
                console.log(this.successCarDriver);
                this.loadCars();
              }, err => {
                console.log(err.error.error);
                this.smsError(err.error.error);
                // loading.dismiss();
              })
          }
        }
      ]
    });

    await alert.present();
  }

  async openModalCarAddEdit(item, condition) {
    const modal = await this.modalController.create({
      component: CreatePage,
      cssClass: 'test-modal',
      componentProps: { condition: condition, item: item },
      // breakpoints:[0.6, 0.5, 1],
      // initialBreakpoint: 1
    })
    await modal.present();
    const data = await modal.onDidDismiss();
    console.log(data.data);
    if (data.data) {
      this.loadCars();
    }
  }

  notifications() {
    this.nav.navigateRoot(["/notifications"]);
  }
}
