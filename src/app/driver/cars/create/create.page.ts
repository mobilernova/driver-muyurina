import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
  imgFoto = "assets/img/logo.png";
  condition: any;
  num_placa: any;
  item:any;
  idDriver: string;
  imgPlaca: string;
  imgAuto: string;
  buttonEnable: boolean;
  createCarSuccess: any;
  
  preFotoPlaca : any;
  preFotoAuto : any;
  constructor(public modal:ModalController,
  private service:ApiService,
  public toast:ToastController,
  private camera: Camera) { 
    this.idDriver = localStorage.getItem("id");
  }

  ngOnInit() {
    if (this.condition == "add") {
      this.num_placa = "";
      this.imgPlaca = "assets/img/logo.png";
      this.imgAuto = "assets/img/logo.png";
      this.buttonEnable = true;
    } else {
      this.num_placa = this.item.placa;
      this.imgPlaca = this.item.foto_placa;
      this.imgAuto = this.item.foto_vehiculo;
      this.buttonEnable = false;

    }
  }

  close() {
    this.modal.dismiss();
  }

  addCar(){
    this.service.createTransporte(this.preFotoPlaca,this.num_placa,this.preFotoAuto,this.idDriver).subscribe(
      async data => {
        this.createCarSuccess = data.data;
        console.log(this.createCarSuccess);
        const toast = await this.toast.create({
          message: 'Auto creado',
          duration: 2000
        });
        toast.present();  
        this.modal.dismiss({
          carCreate:true,
        });;
      }, err => {
        console.log(err.error.error);
      }
    );
  }

  editCar(){
    this.service.updateTransporte(this.preFotoPlaca,this.num_placa,this.preFotoAuto,this.idDriver).subscribe(
      async data => {
        this.createCarSuccess = data.data;
        console.log(this.createCarSuccess);
        const toast = await this.toast.create({
          message: 'Auto Editado',
          duration: 2000
        });
        toast.present();  
        this.modal.dismiss({
          carCreate:true,
        });;
      }, err => {
        console.log(err.error.error);
      }
    );
  }

  getimageAuto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020
    }

    this.camera.getPicture(options).then((imageData) => {
      this.preFotoAuto = "data:image/jpeg;base64," + imageData;
      // this.updatePhotoClient(this.photo);
      // localStorage.setItem('foto', this.photo);
    }, (err) => {
      console.log(err);

    });
  }

  getimagePlacaAuto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020
    }

    this.camera.getPicture(options).then((imageData) => {
      this.preFotoPlaca = "data:image/jpeg;base64," + imageData;
      // this.updatePhotoClient(this.photo);
      // localStorage.setItem('foto', this.photo);
    }, (err) => {
      console.log(err);

    });
  }
}
