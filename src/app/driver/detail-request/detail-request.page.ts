import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../service/api.service';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { CallNumber } from '@awesome-cordova-plugins/call-number/ngx';
import { LaunchNavigator } from '@awesome-cordova-plugins/launch-navigator/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@awesome-cordova-plugins/in-app-browser/ngx';

@Component({
  selector: 'app-detail-request',
  templateUrl: './detail-request.page.html',
  styleUrls: ['./detail-request.page.scss'],
})
export class DetailRequestPage implements OnInit {
  options: InAppBrowserOptions = {
    location: 'no',//Or 'no' 
    hidden: 'yes', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls 
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'yes', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'pagesheet',//iOS only 
    fullscreen: 'yes',//Windows only    
  };

  item: any;
  detailRequest: any;
  idDriver: string;
  successAcceptRequest: any;
  starSuccess: any;
  constructor(private service: ApiService,
    public router: Router,
    public toast: ToastController,
    private callNumber: CallNumber,
    private launchNavigator: LaunchNavigator,
    public inap: InAppBrowser,
    public nav: NavController,
    public alertController: AlertController
  ) {
    this.idDriver = localStorage.getItem("id");
    this.item = this.router.getCurrentNavigation().extras.state.data;
  }

  ngOnInit() {
    console.log(this.item);
    this.loadRequestDetail(this.item.id_solicitud);
  }

  async loadRequestDetail(id) {

    this.service.detalleSolicitudes(id).subscribe(
      async data => {
        this.detailRequest = data.data;
        console.log(this.detailRequest);
      }, err => {
        console.log(err.error.error);
        this.smsError(err.error.error);
        // loading.dismiss();
      })
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  callMe() {
    this.callNumber.callNumber(this.detailRequest.cliente_celular, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  goDestinoRecojo() {
    this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then((isAvailable) => {
      if (isAvailable) {
        this.launchNavigator.navigate([this.detailRequest.lat_origen, this.detailRequest.lng_origen], {
          // start: "" + this.lat + ", " + this.lng,
          app: this.launchNavigator.APP.GOOGLE_MAPS,
        });
      } else {
        this.message('Por favor instale Google Maps para utilizar esta función');
      }
    });
  }

  goDestinoDestino() {
    this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then((isAvailable) => {
      if (isAvailable) {
        this.launchNavigator.navigate([this.detailRequest.lat, this.detailRequest.lng], {
          // start: "" + this.lat + ", " + this.lng,
          app: this.launchNavigator.APP.GOOGLE_MAPS,
        });
      } else {
        this.message('Por favor instale Google Maps para utilizar esta función');
      }
    });
  }

  async message(n) {
    const toast = await this.toast.create({
      message: n,
      duration: 2000
    });
    toast.present();
  }

  goWhatshapp(item) {
    this.inap.create('https://api.whatsapp.com/send?phone=591' + this.detailRequest.cliente_celular + '&text=Hola que tal te saluda ' + this.detailRequest.nombre_rep + '?', `_system`);
  }

  async goHome(item) {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Desea finalizar la carrera?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');

            this.service.finallySolicitudes(this.detailRequest.id, this.idDriver).subscribe(
              async data => {
                this.successAcceptRequest = data.data;
                this.nav.navigateRoot(["/home"]);
                console.log(this.successAcceptRequest);
                this.activateDriver();
              }, err => {
                console.log(err.error.error);
                this.smsError(err.error.error);
                // loading.dismiss();
              })

          }
        }
      ]
    });

    await alert.present();
  }
  // this.nav.navigateRoot(["/home"]);
  // }

  activateDriver() {
    this.service.updateState(1, this.idDriver).subscribe(
      async data => {
        this.starSuccess = data.data;
        const toast = await this.toast.create({
          message: 'El Driver esta "En Servicio"',
          duration: 2000
        });
        toast.present();    
      }, err => {
        console.log(err);
      })
  }
}
