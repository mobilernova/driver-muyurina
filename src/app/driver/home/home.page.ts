import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, LoadingController, MenuController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';
import { ProfilePage } from '../profile/profile.page';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationEvents, BackgroundGeolocationResponse, BackgroundGeolocationAccuracy } from '@awesome-cordova-plugins/background-geolocation/ngx';

import { getDatabase, ref, set } from "firebase/database";
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';

// const config: BackgroundGeolocationConfig = {
//   desiredAccuracy: 0,
//   stationaryRadius: 30,
//   distanceFilter: 5, // Valor Cambiado
//   debug: false, // Emite sonidos
//   interval: 4000,
//   fastestInterval: 7000, // Añadido
//   activitiesInterval: 7000, // Añadido
//   notificationsEnabled: false,
//   stopOnTerminate: false,
//   startForeground: true, // Añadido
//   notificationTitle: 'Tracking',
//   notificationText: 'Tracking de los pedidos'
// };

const config: BackgroundGeolocationConfig = {
  desiredAccuracy: 0,
  stationaryRadius: 10,
  distanceFilter: 5, // Valor Cambiado
  debug: true, // Emite sonidos
  interval: 4000,
  fastestInterval: 5000, // Añadido
  activitiesInterval: 5000, // Añadido
  notificationsEnabled: false,
  stopOnTerminate: false,
  startForeground: true, // Añadido
  notificationTitle: 'Tracking Muyurina',
  notificationText: 'Tracking'
};

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  idDriver: string;
  successDriver: any;
  estado: boolean;
  form: FormGroup;
  foto: any;
  email: any;
  nombre_completo: any;
  starSuccess: any;
  // eventControl: boolean;
  lat: number;
  lng: number;
  photo: string;
  editPhoto: any;
  trackingCondition: any;
  estadoTracking: boolean;
  stateService: any;
  controlEnabled: boolean;
  constructor(public nav: NavController,
    public modalController: ModalController,
    public loadingController: LoadingController,
    private service: ApiService,
    public toast: ToastController,
    public formBuilder: FormBuilder,
    private backgroundGeolocation: BackgroundGeolocation,
    public alertController: AlertController,
    public menu: MenuController,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    private androidPermissions: AndroidPermissions,

  ) {
    this.menu.enable(true);
    this.idDriver = localStorage.getItem("id");
  }

  ngOnInit() {
    console.log(this.lat, this.lng);
    this.lat = parseFloat(localStorage.getItem("lat"));
    this.lng = parseFloat(localStorage.getItem("lng"));
    this.loadProfile();
  }

  async loadProfile() {
    console.log(this.lat,this.lng);
    
    this.trackingCondition = localStorage.getItem("trackingAuth");
    this.form = this.formBuilder.group({
      estado: [this.estado],
      estadoTracking: [this.trackingCondition]
    });
    const loading = await this.loadingController.create({
      spinner: 'bubbles'
    });
    await loading.present().then(() => {
      this.service.getUser(this.idDriver).subscribe(
        async data => {
          this.successDriver = data.data;
          this.foto = this.successDriver.foto;
          this.nombre_completo = this.successDriver.nombre_completo
          this.email = this.successDriver.correo;
          this.stateService = this.successDriver.estado_pedido;
          console.log(this.stateService);

          if (this.successDriver.estado === "Habilitado") {
            this.controlEnabled = true;
          } else {
            this.controlEnabled = false;
          }
          if (this.successDriver.estado_pedido === "En servicio") {
            this.estado = true;
            // this.eventControl = true;
            // this.StartBackgroundGeo('Disponible', this.successDriver.nombre_movil, this.successDriver.id);
          } else {
            this.estado = false;
            // this.eventControl = false;
            // this.backgroundGeolocation.stop();
          }
          if (this.trackingCondition === "true") {
            console.log("entre al tru");
            this.estadoTracking = true;
            this.StartBackgroundGeo('Disponible', this.successDriver.nombre_movil, this.successDriver.id);
          } else {
            console.log("entre al falsito");
            this.estadoTracking = false;
            this.backgroundGeolocation.stop();
          }
          this.form = this.formBuilder.group({
            estado: [this.estado],
            estadoTracking: [this.estadoTracking]
          });
          await loading.dismiss();
        }, err => {
          console.log(err.error.error);
          this.smsError(err.error.error);
          loading.dismiss();
        })
    })
  }

  goDestination(item) {
    console.log(item);
    if (item === 'profile') {
      // this.nav.navigateRoot("/orders")
      this.modalEditProfile();
    } else if (item === 'cars') {
      this.nav.navigateRoot("/cars");
      // } else if (item === 'edit') {
      //   this.nav.navigateRoot("/edit-restaurant");
    } else if (item === 'request') {
      this.nav.navigateRoot("/asigned-request");
    } else if (item === 'information') {
      this.nav.navigateRoot("/informacion");
    }
  }

  async clickCierre(e) {
    console.log(e);
    if (e.detail.checked) {
      console.log("entre al true");
      this.service.updateState(1, this.idDriver).subscribe(
        async data => {
          this.starSuccess = data.data;
          this.estado = true;
          const toast = await this.toast.create({
            message: 'El Driver esta "En Servicio"',
            duration: 2000
          });
          toast.present();
          this.loadProfile();

          this.registerUpdateStop('Disponible', this.successDriver.nombre_movil, this.successDriver.id);
        }, err => {
          console.log(err);
        })
    } else {
      this.service.updateState(0, this.idDriver).subscribe(
        async data => {
          this.starSuccess = data.data;
          this.estado = false;
          const toast = await this.toast.create({
            message: 'El Driver esta "Fuera de Servicio"',
            duration: 2000
          });
          toast.present();
          this.loadProfile();
          this.registerUpdateStop('Ausente', this.successDriver.nombre_movil, this.successDriver.id);
          // console.log(this.starSuccess);
        }, err => {
          console.log(err);

        })
    }

  }

  async modalEditProfile() {
    const modal = await this.modalController.create({
      component: ProfilePage,
      componentProps: { value: 123 }
    });

    await modal.present();
    const data = await modal.onDidDismiss();
    this.loadProfile();
  }

  StartBackgroundGeo(estado, nombre_movil, id_movil) {
    const data = this.androidPermissions.requestPermissions([
      "android.permission.ACCESS_BACKGROUND_LOCATION",
      "android.permission.ACCESS_COARSE_LOCATION",
      "android.permission.ACTIVITY_RECOGNITION",
      this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
    ]);
    this.backgroundGeolocation.configure(config)
      .then(() => {
        this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
          console.log(location);
          this.lat = location.latitude;
          this.lng = location.longitude;
          console.log(this.lat);

          this.register(estado, location, nombre_movil, id_movil);
        });
      });
    this.backgroundGeolocation.start();
  }

  register(estado, location, nombre_movil, id_movil) {
    localStorage.setItem("lat", location.latitude);
    localStorage.setItem("lng", location.longitude);

    const db = getDatabase();

    set(ref(db, 'locations/' + id_movil), {
      estado: estado,
      lat: location.latitude,
      lng: location.longitude,
      nombre_movil: nombre_movil,
      rep_id: id_movil
    });

  }

  StopBackgroundGeo(estado, nombre_movil, id_movil) {
    this.backgroundGeolocation.stop();
    this.registerUpdateStop(estado, nombre_movil, id_movil);
  }

  registerUpdateStop(estado, nombre_movil, id_movil) {
    console.log("entre al update del stop background");

    console.log(this.lat, this.lng);
    console.log("entre aqui");
    const db = getDatabase();

    set(ref(db, 'locations/' + id_movil), {
      estado: estado,
      lat: this.lat,
      lng: this.lng,
      nombre_movil: nombre_movil,
      rep_id: id_movil
    });

  }

  async close() {
    console.log(this.lat);
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Esta seguro que desea cerrar sesión?',
      buttons: [
        {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            // this.StopBackgroundGeo('Ausente', this.successDriver.nombre_movil, this.successDriver.id);
            localStorage.clear();
            this.nav.navigateRoot(["/login"]);
            this.backgroundGeolocation.stop();
            // this.alertController.dismiss();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });
    await alert.present();
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  async tracking(e) {
    console.log(this.stateService);
    if (e.detail.checked) {
      localStorage.setItem("trackingAuth", "true");
      this.estadoTracking = true;
      this.form = this.formBuilder.group({
        estado: [this.estado],
        estadoTracking: [this.estadoTracking]
      });
      const toast = await this.toast.create({
        message: 'El Tracking esta activado"',
        duration: 2000
      });
      toast.present();
      this.StartBackgroundGeo('Disponible', this.successDriver.nombre_movil, this.successDriver.id);

    } else {
      localStorage.setItem("trackingAuth", "false");
      this.estadoTracking = false;
      const toast = await this.toast.create({
        message: 'El tracking esta desactivado',
        duration: 2000
      });
      toast.present();
      this.StopBackgroundGeo('En parada', this.successDriver.nombre_movil, this.successDriver.id);

    }

  }


  async updatePhoto() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Sacar fotografia',
      buttons: [{
        text: 'Camara',
        icon: 'camera',
        handler: () => {
          this.takephoto();
        }
      }, {
        text: 'Galeria',
        icon: 'image',
        handler: () => {
          this.getimage();
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

  takephoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020
    }

    this.camera.getPicture(options).then((imageData) => {
      this.photo = "data:image/jpeg;base64," + imageData;
      this.updatePhotoClient(this.photo);
    }, (err) => {
      console.log(err);

    });
  }
  getimage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit: false,
      targetHeight: 800,
      targetWidth: 1020
    }

    this.camera.getPicture(options).then((imageData) => {
      this.photo = "data:image/jpeg;base64," + imageData;
      this.updatePhotoClient(this.photo);
    }, (err) => {
      console.log(err);

    });
  }
  updatePhotoClient(foto) {
    this.service.updateUser(this.idDriver, '', '', '', '', '', '', '', '', '', foto).subscribe(
      async data => {
        this.editPhoto = data.data;
        console.log(this.editPhoto);
        const toast = await this.toast.create({
          message: 'Foto editada',
          duration: 2000
        });
        toast.present();
        this.loadProfile();
      }, err => {
        console.log(err.error.error);
      }
    );
  }
}
