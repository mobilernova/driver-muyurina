import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  idDriver: string;
  successNotificationsDriver: any;

  constructor(private service:ApiService,
    public toast:ToastController){
    this.idDriver = localStorage.getItem("id");

  }

  ngOnInit() {
    this.loadNotifications();
  }
  
  async loadNotifications(){
    this.service.getNotificaciones(this.idDriver).subscribe(
      async data => {
        this.successNotificationsDriver = data.data;
        console.log(this.successNotificationsDriver);
        
      }, err => {
        console.log(err.error.error);
        this.smsError(err.error.error);
        // loading.dismiss();
      })
  }

  async smsError(sms) {
    const toast = await this.toast.create({
      message: sms,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }
}