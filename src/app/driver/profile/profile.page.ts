import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from '../../service/api.service';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  successRegister: any;
  // img = "assets/img/logo.png";
  fotoDefault: string;
  player_id: string;
  form: FormGroup;
  string: number;
  idDriver: string;
  successDriver: any;
  img:any;
  constructor(public modal:ModalController,
    private service:ApiService,
    public loadingController:LoadingController,
    public toast:ToastController,
    public actionSheetController:ActionSheetController,
    private camera: Camera,
    public formBuilder: FormBuilder,
    public nav:NavController) { 
      this.idDriver = localStorage.getItem("id");
    }




    ngOnInit() {
      //Averiguar mas antes
      this.form = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.email])],
        // password: ['', Validators.compose([Validators.required])],
        nombre: ['', Validators.compose([Validators.required])],
        licencia: ['', Validators.compose([Validators.required])],
        domicilio: ['', Validators.compose([Validators.required])],
        edad: ['', Validators.compose([Validators.required])],
        nombre_movil: ['', Validators.compose([Validators.required])],
        ci: ['', Validators.compose([Validators.required])],
        numero_cel: ['', Validators.compose([Validators.required, Validators.max(79999999), Validators.min(59999999)])],
      });

      this.service.getUser(this.idDriver).subscribe(
        async data => {
          this.successDriver = data.data;
          // this.formBuilder
          this.form.controls['email'].setValue(this.successDriver.correo);
          this.form.controls['nombre'].setValue(this.successDriver.nombre_completo);
          this.form.controls['licencia'].setValue(this.successDriver.licencia);
          this.form.controls['domicilio'].setValue(this.successDriver.domicilio);
          this.form.controls['edad'].setValue(this.successDriver.edad);
          this.form.controls['nombre_movil'].setValue(this.successDriver.nombre_movil);
          this.form.controls['ci'].setValue(this.successDriver.ci);
          this.form.controls['numero_cel'].setValue(this.successDriver.numero_cel);

          this.img = this.successDriver.foto;
          console.log(this.successDriver);
          // this.formNew();
        }, err => {
          console.log(err.error.error);
        })
     
    }


  
    numberOnlyValidation(event: any) {
      const pattern = /[0-9]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  
    onChangeTime(e) {
      this.string = (8 - e.detail.value.length);
    }
  
    public checkError = (controlName: string, errorName: string) => {
      return this.form.controls[controlName].hasError(errorName);
    }
    async goRegister() {
      this.player_id = localStorage.getItem("player_id");
        this.service.updateUser(this.idDriver,this.form.value.nombre,this.form.value.edad,this.form.value.domicilio,this.form.value.email,this.form.value.ci,this.form.value.numero_cel,this.form.value.licencia,this.form.value.nombre_movil,this.player_id,this.fotoDefault).subscribe(
          async data => {
            console.log(data);
            
            this.successRegister = data.data;
            this.modal.dismiss();
            this.ok();
          }, err => {
            //  
            if (err.error.code == 422) {
              this.smsError("Se debe indicar al menos un valor diferente para actualizar.");
            } else if(err.error.errors.player_id == 'The player id has already been taken.') {
              
            }else if(err.error.error){
              this.smsError("Email, Carnet de identidad o Número de celular ya fueron tomados");
            }
            console.log(err);
           
          
          }
        );

    }
  
  
    async updatePhoto() {
      const actionSheet = await this.actionSheetController.create({
        header: 'Sacar fotografia',
        buttons: [{
          text: 'Camara',
          icon: 'camera',
          handler: () => {
            this.takephoto();
          }
        }, {
          text: 'Galeria',
          icon: 'image',
          handler: () => {
            this.getimage();
          }
        }, {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
  
      await actionSheet.present();
    }
  
    takephoto() {
      const options: CameraOptions = {
        quality: 70,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        allowEdit: false,
        targetHeight: 800,
        targetWidth: 1020
      }
  
      this.camera.getPicture(options).then((imageData) => {
        this.fotoDefault = "data:image/jpeg;base64," + imageData;
        // this.updatePhotoClient(this.photo);
      }, (err) => {
        console.log(err);
  
      });
    }
    getimage() {
      const options: CameraOptions = {
        quality: 70,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        saveToPhotoAlbum: false,
        correctOrientation: true,
        allowEdit: false,
        targetHeight: 800,
        targetWidth: 1020
      }
  
      this.camera.getPicture(options).then((imageData) => {
        this.fotoDefault = "data:image/jpeg;base64," + imageData;
        // this.updatePhotoClient(this.photo);
      }, (err) => {
        console.log(err);
  
      });
    }
  
  
    async ok() {
      const toast = await this.toast.create({
        message: 'Perfil editado',
        duration: 3000,
        cssClass: 'my-custom-class-success'
      });
      toast.present();
    }
  
    async smsError(sms) {
      const toast = await this.toast.create({
        message: sms,
        duration: 5000,
        cssClass: 'my-custom-class-alert'
      });
      toast.present();
    }

  close(){
    this.modal.dismiss();
  }

}
