import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;

  constructor(private activatedRoute: ActivatedRoute,private oneSignal: OneSignal) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');

    this.oneSignal.startInit('2d404858-12d5-41cc-8696-e1881568fee4', '178883670824');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    this.oneSignal.endInit();
    this.oneSignal.getIds().then(data => {
      console.log(data);
      
      localStorage.setItem('player_id', data.userId);
    });
  }

}
