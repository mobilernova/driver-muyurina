import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  path = "https://apimuyurina.rnova-services.com/api";
  accesToken: string;
  httpOptions: { headers: HttpHeaders; };
  httpOptions2: { headers: HttpHeaders; };
  urls: string;

  constructor(private http: HttpClient) { }


  loginCliente(username, password, player_id): Observable<any> {
    var datoaEnviar = {
      "correo": username,
      "password": password,
      "player_id": player_id
    }
    return this.http.post(this.path + "/login-rep", datoaEnviar, this.httpOptions2)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

   registerCliente(nombre_completo,edad,domicilio,correo,ci, numero_cel,licencia, nombre_movil,player_id,foto): Observable<any> {
    var datoaEnviar = {
    "nombre_completo": nombre_completo,
    "edad": edad,
    "domicilio":domicilio ,
    "correo": correo,
    "ci": ci,
    "numero_cel": numero_cel,
    "licencia": licencia,
    "nombre_movil": nombre_movil,
    "player_id":player_id,
    "foto":foto
    }
    return this.http.post(this.path + "/registro-taxi", datoaEnviar, this.httpOptions2)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  getUser(idRepartidor): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };
    return this.http.get(this.path + "/detalle-repartidor/" + idRepartidor + "", this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }


  updateUser(idRepartidor, nombre_completo = "", edad: "", domicilio = "",correo,ci, numero_cel,licencia, nombre_movil,player_id,foto): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "nombre_completo": nombre_completo,
    "edad": edad,
    "domicilio":domicilio ,
    "correo": correo,
    "ci": ci,
    "numero_cel": numero_cel,
    "licencia": licencia,
    "nombre_movil": nombre_movil,
    "player_id":player_id,
      "foto": foto,
    }

    return this.http.put(this.path + "/update-perfil/" + idRepartidor + "", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  getTransportes(repartidores_id): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    console.log(this.accesToken);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "repartidores_id": repartidores_id
    }

    return this.http.post(this.path + "/transportes-rep", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  createTransporte(foto_placa = "", placa: "", foto_vehiculo = "", repartidores_id = ""): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "foto_placa": foto_placa,
      "placa": placa,
      "foto_vehiculo": foto_vehiculo,
      "repartidores_id": repartidores_id,
    }

    return this.http.post(this.path + "/transporte-create", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  updateTransporte(foto_placa = "", placa: "", foto_vehiculo = "", repartidores_id = ""): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "foto_placa": foto_placa,
      "placa": placa,
      "foto_vehiculo": foto_vehiculo,
      "repartidores_id": repartidores_id,
    }

    return this.http.put(this.path + "/transporte-update/"+repartidores_id+"", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  updateTransporteSelect(id_repartidor, id_transporte): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "id_repartidor": id_repartidor,
      "id_transporte": id_transporte
    }

    return this.http.post(this.path + "/transporte-select", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  getNotificaciones(idRepartidor): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };
    return this.http.get(this.path + "/notificaciones-repartidor/" + idRepartidor + "", this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  getsolicitudes(idRepartidor,modo): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "id_repartidor": idRepartidor,
      "modo": modo,
     
    }

    return this.http.post(this.path + "/list-solicitudes",datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  acceptsolicitudes(solicitudes_id,repartidores_id): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "solicitudes_id": solicitudes_id,
      "repartidores_id": repartidores_id,
     
    }

    return this.http.post(this.path + "/aceptar-solicitud",datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  detalleSolicitudes(solicitudes_id): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    return this.http.get(this.path + "/detalle-solicitud/"+solicitudes_id+"", this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  finallySolicitudes(pedidos_id,repartidores_id): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "solicitudes_id": pedidos_id,
      "repartidores_id":repartidores_id
    }

    return this.http.post(this.path + "/fin-solicitud",datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  
  updateState(estado,id): Observable<any> {
    this.accesToken = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accesToken
      })
    };

    var datoaEnviar = {
      "estado": estado,
    }

    return this.http.put(this.path + "/edit-disponibilidad/"+id+"",datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }
}